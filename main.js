
      console.log(ol)

const raster = new ol.layer.Tile({
  source: new ol.source.OSM(),
});

const source = new ol.source.Vector();
const vector = new ol.layer.Vector({
  source: source,
  style: new ol.style.Style({
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 255, 0.2)',
    }),
    stroke: new ol.style.Stroke({
      color: '#ffcc33',
      width: 2,
    }),
    image: new ol.style.Circle({
      radius: 7,
      fill: new ol.style.Fill({
        color: '#ffcc33',
      }),
    }),
  }),
});
// minimap 

const overviewMapControl = new ol.control.OverviewMap({
  // see in overviewmap-custom.html to see the custom CSS used
  className: 'ol-overviewmap ol-custom-overviewmap',
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM({
        'url':
          'https://{a-c}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png',
      }),
    }),
  ],
  collapseLabel: '\u00BB',
  label: '\u00AB',
  collapsed: false,
});



const extent = [0, 0, 1024, 968];
const projection = new ol.proj.Projection({
  code: "xkcd-image",
  units: "pixels",
  extent: extent,
});

const map = new ol.Map({
  controls: ol.control.defaults().extend([overviewMapControl]),
  interactions: ol.interaction.defaults(),
  layers: [
    new ol.layer.Image({
      source: new ol.source.ImageStatic({
        url: "/home/rose/projects/openlayers-examples/hamamatsu_02-region.jpg",
        projection: projection,
        imageExtent: extent,
      }),
    }),
    // [
    //   new ol.layer.Tile({
    //     source: new ol.source.OSM({
    //       'url':
    //         'https://{a-c}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png' +
    //         '?apikey=Your API key from https://www.thunderforest.com/docs/apikeys/ here',
    //     }),
    //   }),
    // ],
    raster, vector
  ],
  target: "map",
  view: new ol.View({
    projection: projection,
    center: ol.extent.getCenter(extent),
    zoom: 2,
    maxZoom: 8,
  }),
});




//scaleLine
const scaleLine = new ol.control.ScaleLine({bar: true, text: true, minWidth: 125});
map.addControl(scaleLine);
const modify = new ol.interaction.Modify({source: source});
map.addInteraction(modify);

let draw, snap; // global so we can remove them later
const typeSelect = document.getElementById('type');

function addInteractions() {
  draw = new ol.interaction.Draw({
    source: source,
    type: typeSelect.value,
  });
  map.addInteraction(draw);
  snap = new ol.interaction.Snap({source: source});
  map.addInteraction(snap);
}

/**
 * Handle change event.
 */
typeSelect.onchange = function () {
  map.removeInteraction(draw);
  map.removeInteraction(snap);
  addInteractions();
};

addInteractions();

